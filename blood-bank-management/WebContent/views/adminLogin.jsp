<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Page</title>
</head>
<body>
<h1>Welcome to Admin Login</h1>
<h3>${erc}</h3>
<s:form action="admin" method="post" modelAttribute="a">
User Name:<s:input path="username" autocomplete="off"></s:input> <br/>
Password:<s:input path="password" autocomplete="off"></s:input><br/>
<s:button>login</s:button>
</s:form>
</body>
</html>