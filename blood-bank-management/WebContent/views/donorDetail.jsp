<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Donor Page</title>
</head>
<body>
<h1>Add Donor Detail</h1><br>
<a href="serach">Search Request</a>
<hr>
<s:form action="donordata" method="post" modelAttribute="dd">
<h4>${ef}</h4>
Name:<s:input path="name" autocomplete="off"></s:input><br/>
Date Of Birth:<s:input path="dob" autocomplete="off"></s:input><br/>
Blood Group:<s:input path="bloodGroup" autocomplete="off"></s:input> <br/>
Address:<s:input path="address" autocomplete="off"></s:input><br/>
Contact:<s:input path="contact" autocomplete="off"></s:input><br/>
Gender:<s:input path="gender" autocomplete="off"></s:input><br/>
<s:button>submit</s:button>
</s:form>
</body>
</html>