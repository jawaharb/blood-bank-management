<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Donor page</title>
</head>
<body>
<h1>Welcome to Donor Register Page</h1>
<hr>
<h1>Register</h1>
<s:form action="donorAdd" method="post" modelAttribute="d">
First Name:<s:input path="firstname" autocomplete="off"></s:input><br/>
Last Name:<s:input path="lastname" autocomplete="off"></s:input><br/>
User Name:<s:input path="username" autocomplete="off"></s:input> <br/>
Password:<s:input path="password" autocomplete="off"></s:input><br/>
<s:button>register</s:button>
</s:form>
</body>
</html>