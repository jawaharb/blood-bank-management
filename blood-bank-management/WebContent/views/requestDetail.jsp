<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Request</title>
</head>
<body>
<h1>Welcome To Add request</h1><br>
<hr/>
<s:form action="requestData" method="post" modelAttribute="rd">
<h3>${eff}</h3>
Patient Name:<s:input path="patientName"></s:input><br/>
Required Blood Group:<s:input path="requiredBloodGroup"></s:input><br/>
City:<s:input path="city"></s:input><br/>
Doctor Name:<s:input path="doctorName"></s:input><br/>
Hospital Name:<s:input path="hospitalName"></s:input><br/>
Hospital Address:<s:input path="hospitalAddress"></s:input><br/>
Date Of Blood Needed:<s:input path="bloodDate"></s:input><br/>
Contact Name:<s:input path="contactName"></s:input><br/>
Contact Number:<s:input path="contactNumber"></s:input><br/>
Contact Email Id:<s:input path="emailId"></s:input><br/>
<s:button>submit</s:button>
</s:form>
<script> 
function submit() { 
  /*Put all the data posting code here*/ 
 document.getElementByaction("requestData").reset(); 
} 
</script> 
</body>
</html>