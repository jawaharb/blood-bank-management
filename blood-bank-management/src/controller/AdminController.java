package controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.IAdmin;
import model.Admin;
import model.IAdminImp;



public class AdminController {
	IAdmin dao=new IAdminImp();

	@RequestMapping("adminLogin")
	public ModelAndView adminValid() {
		return new ModelAndView("adminLogin", "a", new Admin());
	}
	@RequestMapping("admin")
	public ModelAndView adminLoginValid(@ModelAttribute("a") Admin admin) {
		boolean isValid = dao.adminAuth(admin);
		if (isValid) {
			return new ModelAndView("adminView","av", new Admin());
		} else {
			return new ModelAndView("adminLogin","erc", "invalid o/p");
		}
}
}
