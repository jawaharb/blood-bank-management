package controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.IDonorReg;
import dao.IDonorRegImp;
import model.DonorInfo;
import model.DonorReg;
import model.RequestInfo;

@Controller
public class DonorRegController {
	
	IDonorReg dao = new IDonorRegImp();
	@RequestMapping("donorRegister")
	public ModelAndView insertDonorReg() {
		return new ModelAndView("donorRegister","d", new DonorReg());
	}
	@RequestMapping("donorAdd")
	public ModelAndView add(@ModelAttribute("d") DonorReg d) {
		dao.insert(d);
		return new ModelAndView("display");
	}
	
	@RequestMapping("donorLogin")
	public ModelAndView adminValid() {
		return new ModelAndView("donorLogin", "l", new DonorReg());
	}
	@RequestMapping("donor")
	public ModelAndView adminLoginValid(@ModelAttribute("l") DonorReg l) {
		boolean isValid = dao.donorAuthen(l);
		if (isValid) {
			return new ModelAndView("donorDetail","dd", new DonorInfo());
		} else {
			return new ModelAndView("donorLogin","e", "invalid o/p");
		}
	}
	@RequestMapping("donorDetail")
	public ModelAndView adddonor() {
		return new ModelAndView("donorDetail","dd", new DonorInfo());
	}
	@RequestMapping("donordata")
	public ModelAndView add1(@ModelAttribute("dd") DonorInfo donor) {
		dao.addDonor(donor);
		return new ModelAndView("donorDetail","ef","detail sumbited successfully!!!");
	}
	@RequestMapping("view")
	public ModelAndView view() {
		List<RequestInfo> list= dao.view();
		return new ModelAndView("donorView","v",list);
	}
}

	
	
	
	