package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.IRequestReg;
import dao.IRequestRegImp;
import model.RequestInfo;
import model.RequestReg;

@Controller
public class RequestRegController {
	IRequestReg dao = new IRequestRegImp();
	
	@RequestMapping("requestRegister")
	public ModelAndView insertRequest() {
		return new ModelAndView("requestRegister","ra", new RequestReg());
		
	}
	@RequestMapping("requesterAdd")
	public ModelAndView add(@ModelAttribute("ra") RequestReg ra) {
		dao.insert(ra);
		return new ModelAndView("display");
	}
	
	@RequestMapping("requestLogin")
	public ModelAndView requestValid() {
		return new ModelAndView("requestLogin","r", new RequestReg());
	}
	@RequestMapping("register")
	public ModelAndView requestLog(@ModelAttribute("r") RequestReg r) {
		boolean isValid=dao.requestAuthen(r);
		if (isValid) {
			return new ModelAndView("requestDetail","rd", new RequestInfo());
		} else {
			return new ModelAndView("requestLogin","e", "invalid o/p");
		}
		
	}
	@RequestMapping("requestDetail")
	public ModelAndView requestAdd() {
		return new ModelAndView("requestDetail","rd", new RequestInfo());	
	}
	@RequestMapping("requestData")
	public ModelAndView add1(@ModelAttribute("rd") RequestInfo mo) {
		dao.addRequest(mo);
		return new ModelAndView("requestDetail","eff","detail sumbited successfully!!!");
	}
}
