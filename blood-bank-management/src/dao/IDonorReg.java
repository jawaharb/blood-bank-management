package dao;

import java.util.List;

import model.DonorInfo;
import model.DonorReg;
import model.RequestInfo;

public interface IDonorReg {
	public void insert(DonorReg dr);
	public boolean donorAuthen(DonorReg dr);
	public void addDonor(DonorInfo di);
	public List<RequestInfo> view();
}
