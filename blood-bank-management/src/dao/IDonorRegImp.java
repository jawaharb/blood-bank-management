package dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.DonorInfo;
import model.DonorReg;
import model.RequestInfo;

public class IDonorRegImp implements IDonorReg{
	
	
	@Override
	public void insert(DonorReg dr) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		session.save(dr);
		tx.commit();
		
	}
	@SuppressWarnings("rawtypes")
	@Override
	public boolean donorAuthen(DonorReg dr) {
		boolean check=false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q=session.createQuery("from DonorReg where username=?1 and password=?2");
		q.setParameter(1, dr.getUsername());
		q.setParameter(2, dr.getPassword());
		
		List list =q.list();
		if(list!=null && list.size()>0) {
			check=true;
		}
		return check;
	}
	@Override
	public void addDonor(DonorInfo di) {
		
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		session.save(di);
		tx.commit();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	
	@Override
	public List<RequestInfo> view() {
		List<RequestInfo> list = new ArrayList<RequestInfo>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Query q =session.createQuery("from RequestInfo");
		List<RequestInfo> l = q.list();
		for(RequestInfo request :l) {
			list.add(request);
		}
		return list;
	}

}
