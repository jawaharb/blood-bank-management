package dao;

import model.RequestInfo;
import model.RequestReg;

public interface IRequestReg {
	public void insert(RequestReg rr);
	public boolean requestAuthen(RequestReg rr);
	public void addRequest(RequestInfo mode);
}
