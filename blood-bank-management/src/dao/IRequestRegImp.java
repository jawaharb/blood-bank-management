package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.RequestInfo;
import model.RequestReg;

public class IRequestRegImp implements IRequestReg{

	@Override
	public void insert(RequestReg rr) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		session.save(rr);
		tx.commit();
		
		
	}
	@SuppressWarnings("rawtypes")
	@Override
	public boolean requestAuthen(RequestReg rr) {
		boolean check=false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q=session.createQuery("from RequestReg where username=?1 and password=?2");
		q.setParameter(1, rr.getUsername());
		q.setParameter(2, rr.getPassword());
		
		List list =q.list();
		if(list!=null && list.size()>0) {
			check=true;
		}
		return check;
	}
	@Override
	public void addRequest(RequestInfo mode) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx=session.beginTransaction();
		session.save(mode);
		tx.commit();
		
	}
	

}
