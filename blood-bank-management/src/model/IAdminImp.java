package model;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import dao.IAdmin;

public class IAdminImp implements IAdmin{
	@SuppressWarnings("rawtypes")
	@Override
	public boolean adminAuth(Admin ad) {
		boolean check=false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Query q=session.createQuery("from Admin where username=?1 and password=?2");
		q.setParameter(1, ad.getUsername());
		q.setParameter(2,ad.getPassword());
		
		List list =q.list();
		if(list!=null && list.size()>0) {
			check=true;
		}
		return check;
	}

}
