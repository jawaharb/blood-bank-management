package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RequestInfo {
	@Id
	@Column(name="patient_name")
	private String patientName;
	@Column(name="required_blood_group")
	private String requiredBloodGroup;
	private String city;
	@Column(name="doctor_name")
	private String doctorName;
	@Column(name="hospital_name")
	private String hospitalName;
	@Column(name=" hospital_address")
	private String hospitalAddress;
	@Column(name="date_of_blood_needed")
	private String bloodDate;
	@Column(name="contact_name")
	private String contactName;
	@Column(name="contact_number")
	private String contactNumber;
	@Column(name=" contact_email_id")
	private String emailId;
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getRequiredBloodGroup() {
		return requiredBloodGroup;
	}
	public void setRequiredBloodGroup(String requiredBloodGroup) {
		this.requiredBloodGroup = requiredBloodGroup;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getBloodDate() {
		return bloodDate;
	}
	public void setBloodDate(String bloodDate) {
		this.bloodDate = bloodDate;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
}
